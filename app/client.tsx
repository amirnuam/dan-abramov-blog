"use client";

import { FC, useMemo } from "react";
import { useRouter } from "next/navigation";
import clsx from "clsx";
import { useSelector } from "react-redux";

import { Blog } from "app/models";
import { Header, Card } from "app/ui";
import { RootState } from "app/store";
import { formatDate, generateDateFromId, getRandomDate } from "app/utils";

const HomeClientPage: FC<{ blogs: Array<Blog> }> = ({ blogs }) => {
  const router = useRouter();
  const darkMode = useSelector((state: RootState) => state.darkMode.darkMode);

  const sortedBlogs = useMemo(() => {
    const blogsWithDate = blogs.map((blog) => {
      return {
        ...blog,
        date: generateDateFromId(blog.id),
      };
    });
    return blogsWithDate.sort(
      (a, b) => new Date(b.date).getTime() - new Date(a.date).getTime()
    );
  }, [blogs]);

  return (
    <main
      className={clsx(
        "flex min-h-screen max-w-5xl flex-col items-center justify-between p-24 w-full bg-white",
        { "bg-black": darkMode }
      )}
    >
      <Header />
      <div className="w-full items-center justify-between">
        {sortedBlogs.map(({ title, body, id, date }, idx) => (
          <Card
            title={title}
            description={body}
            date={formatDate(date)}
            titleVariant={idx ? "secondary" : "primary"}
            onClick={() => router.push(`/${id}`)}
          />
        ))}
      </div>
    </main>
  );
};

export default HomeClientPage;
