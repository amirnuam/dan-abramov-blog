import { Metadata } from "next";

import { Request } from "app/api";
import { ENDPOINTS } from "app/constants";
import { Blog } from "app/models";

import HomeClientPage from "./client";

export const metadata: Metadata = {
  title: "blog",
  description: "released blogs",
};

export const revalidate = 5000;

const fetchBlogs = async () => {
  const blogs = await Request.get(ENDPOINTS.BLOGS as string)();
  return blogs;
};

export default async function Home() {
  const blogs = (await fetchBlogs()) as Array<Blog>;

  return <HomeClientPage blogs={blogs} />;
}
