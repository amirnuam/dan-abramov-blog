import { Metadata } from "next";

import SingleBlogClientPage from "./client";

export type SingleBlogParams = Record<"id", string>;

export const metadata: Metadata = {
  title: "single blog",
  description: "single released blogs",
};

export default async function SingleBlog({
  params,
}: {
  params: SingleBlogParams;
}) {
  return <SingleBlogClientPage id={params.id} />;
}
