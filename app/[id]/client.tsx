"use client";
import { FC } from "react";

import { useSingleBlog } from "app/queries";
import { Card } from "app/ui";
import { QUERY_KEYS } from "app/constants";
import { formatDate, generateDateFromId } from "app/utils";

import { SingleBlogParams } from "./page";

const SingleBlogClientPage: FC<SingleBlogParams> = ({ id }) => {
  const { data, isLoading } = useSingleBlog(
    { id },
    { enabled: !!id, queryKey: QUERY_KEYS.SINGLE_BLOG }
  );

  if (!isLoading && id && data)
    return (
      <div className="flex w-full h-screen bg-white justify-center">
        <div className="flex flex-col max-w-5xl">
          <Card
            title={data.title}
            description={data.body}
            date={formatDate(generateDateFromId(Number(id)))}
          />
        </div>
      </div>
    );
  return <>...LOADING</>;
};

export default SingleBlogClientPage;
