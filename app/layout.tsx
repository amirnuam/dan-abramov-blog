import type { Metadata } from 'next'
import clsx from "clsx";
import { Inter } from "next/font/google";

import { Wrapper } from "app/ui";

import "./globals.css";

const inter = Inter({
  subsets: ["latin"],
  weight: ["100", "200", "300", "400", "500", "600", "700", "800", "900"],
  adjustFontFallback: true,
  display: "swap",
});

export const metadata: Metadata = {
  title: "dan abramov",
  description: "dan abramov recreated blog",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={clsx(inter.className, "flex justify-center")}>
        <Wrapper>{children}</Wrapper>
      </body>
    </html>
  );
}
