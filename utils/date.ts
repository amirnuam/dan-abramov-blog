import { IOption } from "./types";

export function getRandomDate(shouldFormat: boolean = false) {
  const start = new Date(`2023-01-01`).getTime();
  const end = new Date("2023-12-31").getTime();
  const randomTime = start + Math.random() * (end - start);
  return shouldFormat
    ? formatDate(new Date(randomTime).toISOString())
    : new Date(randomTime).toISOString();
}

export function formatDate(dateString: string) {
  const date = new Date(dateString);
  const options: IOption = { month: "long", day: "numeric", year: "numeric" };
  //@ts-ignore
  return new Intl.DateTimeFormat("en-US", options).format(date);
}

export function generateDateFromId(postId: number) {
  const year = 2023;
  const month = postId % 12 || 12;
  const day = postId % 28 || 28;
  return new Date(`${year}-${month}-${day}`).toISOString();
}
