export type IOption = {
  month: string;
  day: string;
  year: string;
};
