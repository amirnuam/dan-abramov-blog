import type { QueryFunctionContext } from "@tanstack/react-query";
import { useQuery } from "@tanstack/react-query";

import { Request } from "app/api";
import { ENDPOINTS, QUERY_KEYS, QueryOption } from "app/constants";
import { Blog } from "app/models";

import type { ISingleBlogArgs } from "./types";

async function getSingleBlog(
  ctx: QueryFunctionContext,
  args: ISingleBlogArgs
): Promise<Blog> {
  const { ...rest } = args;
  const response = await Request.get(ENDPOINTS.SINGLE_BLOG(args.id), {
    signal: ctx.signal,
    params: rest,
  })();

  return response as Blog;
}

export function useSingleBlog(
  args: ISingleBlogArgs,
  options?: QueryOption<Blog>
) {
  return useQuery({
    queryKey: [QUERY_KEYS.SINGLE_BLOG, args],
    queryFn: (ctx) => getSingleBlog(ctx, args),
    ...options,
  });
}
