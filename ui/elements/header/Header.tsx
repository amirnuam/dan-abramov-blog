"use client";

import React, { FC } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Switch, Typography } from "app/ui";
import { RootState } from "app/store";
import { toggleDarkMode } from "app/store/slices";

const Header: FC = () => {
  const dispatch = useDispatch();
  const darkMode = useSelector((state: RootState) => state.darkMode.darkMode);

  const handleToggleSwitch = () => {
    dispatch(toggleDarkMode());
  };

  return (
    <div className="w-full items-center justify-between flex">
      <div className="flex flex-col">
        <Typography className="justify-center text-2xl font-bold pb-6 pt-8">
          overreacted
        </Typography>
      </div>
      <div className="flex flex-col">
        <Switch checked={darkMode} onChange={handleToggleSwitch} />
      </div>
    </div>
  );
};

export default Header;
