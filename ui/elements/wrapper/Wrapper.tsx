"use client";

import React, { FC } from "react";
import { QueryClientProvider } from "@tanstack/react-query";
import { Provider } from "react-redux";

import { store } from "app/store";
import { queryClient } from "app/constants";

const Wrapper: FC<{ children: React.ReactNode }> = ({ children }) => {
  return (
    <QueryClientProvider client={queryClient}>
      <Provider store={store}>{children}</Provider>
    </QueryClientProvider>
  );
};

export default Wrapper;
