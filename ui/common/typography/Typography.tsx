import clsx from "clsx";
import React, { FC } from "react";
import { useSelector } from "react-redux";

import { RootState } from "app/store";

const TEXT_COLORS: Record<string, { dark: string; light: string }> = {
  base: {
    light: "text-black",
    dark: "text-white",
  },
  primary: {
    light: "text-pink",
    dark: "text-darkpink",
  },
  secondary: {
    light: "text-purple",
    dark: "text-darkpurple",
  },
};

type Props = {
  children: React.ReactNode;
  className?: string;
  variant?: keyof typeof TEXT_COLORS;
};

const Typography: FC<Props> = ({ children, className, variant = "base" }) => {
  const mode = useSelector((state: RootState) => state.darkMode.darkMode)
    ? "dark"
    : "light";

  const additionalClassname = TEXT_COLORS[variant][mode];

  return (
    <text className={clsx(className, additionalClassname)}>{children}</text>
  );
};

export default Typography;
