"use client";

import { FC } from "react";

import { Typography } from "..";

type Props = {
  title: string;
  date: string;
  description: string;
  mode?: "light" | "dark";
  titleVariant?: "primary" | "secondary";
  onClick?: () => void;
};

const Card: FC<Props> = ({
  title,
  date,
  description,
  titleVariant,
  onClick,
}) => {
  return (
    <div
      onClick={onClick}
      className={
        "flex flex-col hover:scale-105 my-8 cursor-pointer duration-150"
      }
    >
      <Typography variant={titleVariant} className="text-3xl font-bold my-2">
        {title}
      </Typography>
      <Typography className="text-md">{date}</Typography>
      <Typography className="text-md">{description}</Typography>
    </div>
  );
};

export default Card;
