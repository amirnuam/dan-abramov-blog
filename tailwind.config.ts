import type { Config } from 'tailwindcss'

const config: Config = {
  content: ["./ui/**/*.{js,ts,jsx,tsx,mdx}", "./app/**/*.{js,ts,jsx,tsx,mdx}"],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      colors: {
        pink: "#F3659E",
        purple: "#B54A99",
        darkpink: "#FEB3D8",
        darkpurple: "#E8B4EE",
      },
    },
  },
  plugins: [],
};
export default config
