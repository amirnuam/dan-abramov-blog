import { UseQueryOptions } from "@tanstack/react-query";

enum QUERY_KEYS {
  BLOG = "BLOG",
  SINGLE_BLOG = "SINGLE_BLOG",
}

export default QUERY_KEYS;

export type QueryOption<TData, TError = unknown> = Omit<
  UseQueryOptions<TData, TError, TData, any>,
  "initialData"
> & {
  initialData?: () => undefined;
};
