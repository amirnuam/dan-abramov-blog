import { EndpointsRecord } from "./types";

const ENDPOINTS: EndpointsRecord = {
  BLOGS: "/posts",
  SINGLE_BLOG: (id: string) => `/posts/${id}`,
};

export default ENDPOINTS;
