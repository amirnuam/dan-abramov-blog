type Endpoints = {
  BLOGS: string;
  SINGLE_BLOG: (id: string) => string;
};

export type EndpointsRecord = Record<keyof Endpoints, any>;
